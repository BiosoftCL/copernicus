<?php

namespace Geonodo\Copernicus\Http\Controllers;

use Carbon\Carbon;
use Geonodo\Domain\Model\Instance;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ServerException;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Storage;
use SebastianBergmann\Environment\Console;

class CopernicusController extends Controller
{
    protected $url_copernicus= "https://scihub.copernicus.eu/dhus";
    protected $username="targuelles";
    protected $password="T0m4s@123";
    /**
     * Index view
     *
     * @param Instance $instance
     *
     * @return mixed
     */
    public function index(Instance $instance)
    {
        return view('copernicus::index', ['langs' => Lang::get('copernicus::attributes')]);
    }
    /**
     * Index view
     *
     * @param Instance $instance
     *
     * @return mixed
     */
    public function search(Request $request, Instance $instance)
    {
        $respuesta = [
            'products' => [],
        ];
        //https://scihub.copernicus.eu/dhus/api/stub/products?filter=sea&offset=0&limit=25&sortedby=ingestiondate&order=desc
        $url = $this->url_copernicus.'/search';

        $response = $this->makeRequest('GET', $url, 
        [
            'q' => $request->query('query', '*'),
            'start' => $request->query('start', 0),
            'rows' => $request->query('per_page', 10)
        ]);
       
        if($response->getStatusCode() == '200') {
            $content = $response->getBody()->getContents();
            $copernicusResponse = simplexml_load_string($content);
            $respuesta['totalResults'] = (integer) $copernicusResponse->xpath('opensearch:totalResults')[0];
            $respuesta['startIndex'] = (integer) $copernicusResponse->xpath('opensearch:startIndex')[0];
            $respuesta['itemsPerPage'] = (integer) $copernicusResponse->xpath('opensearch:itemsPerPage')[0];
            foreach($copernicusResponse->entry as $e) {
                $temp = [
                    'title' => (string) $e->title,
                ];                
                foreach($e->link as $link) {
                    if($link->attributes()->rel == 'icon')
                        $temp['icon'] = (string) $link->attributes()->href;
                }
                foreach($e->str as $property) {
                    $temp[(string) $property->attributes()->name] = (string) $property;
                }
                foreach($e->int as $property) {
                    $temp[(string) $property->attributes()->name] = (integer) $property;
                }
                foreach($e->date as $property) {
                    $temp[(string) $property->attributes()->name] = new Carbon((string) $property);
                }
                array_push($respuesta['products'], $temp);
            }
        }
        return $respuesta;
    }

    private function makeRequest($method, $url, $query = []) {
        $client = new Client();
        $response = $client->request($method,$url, [
            'query' => $query,
            'auth' => [$this->username, $this->password]
        ]);
        return $response;

    }

    public function getThumbnail(Request $request, Instance $instance, $uuid) {
        
        $url = "https://scihub.copernicus.eu/dhus/odata/v1/Products('".$uuid."')/Products('Thumbnail')/\$value";
        try {
            $response = $this->makeRequest("GET",$url);
        } catch (ServerException $e) {
            return '';
        }
        if($response->getStatusCode() == '200') {
            $content = $response->getBody()->getContents();
            return response($content)->header('Content-type','image/png') ;
        }
        return '';
    }

    public function getQuicklook(Request $request, Instance $instance, $uuid) {
        $url = "https://scihub.copernicus.eu/dhus/odata/v1/Products('".$uuid."')/Products('Quicklook')/\$value";
        try {
            $response = $this->makeRequest("GET",$url);
        } catch (ServerException $e) {
            return '';
        }
        if($response->getStatusCode() == '200') {
            $content = $response->getBody()->getContents();
            return response($content)->header('Content-type','image/png') ;
        }
        return '';
    }

    public function downloadFile(Request $request, Instance $instance, $uuid) {
        $url = "https://scihub.copernicus.eu/dhus/odata/v1/Products('".$uuid."')/\$value";
        try {
            $response = $this->makeRequest("GET",$url);
        } catch (ServerException $e) {
            return '';
        }

        if($response->getStatusCode() == '200') {
            $content = $response->getBody()->getContents();
            return response($content, 200, $response->getHeaders());
        }
        return '';
    }
}
