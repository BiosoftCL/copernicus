<?php

namespace Geonodo\Copernicus;

use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;


class CopernicusServiceProvider extends ServiceProvider
{
    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishConfig();
        $this->registerRoutes();
        $this->registerResources();
        $this->registerTranslations();
        $this->loadMigrations();
        $this->publishAssets();
        $this->register();
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if (!defined('COPERNICUS_PATH')) {
            define('COPERNICUS_PATH', realpath(__DIR__.'/../'));
        }
    }

    /**
     * Setup the configuration.
     *
     * @return void
     */
    protected function publishConfig()
    {
        $this->publishes([
            __DIR__ . '/../config/copernicus.php' => config_path('copernicus.php'),
        ]);
    }

    /**
     * Register the resources.
     *
     * @return void
     */
    protected function registerResources()
    {
        $this->loadViewsFrom(__DIR__ . '/../resources/views', 'copernicus');
    }

    /**
     * Register the routes and breadcrumbs.
     *
     * @return void
     */
    protected function registerRoutes()
    {
        Route::group([
            'prefix'     => '/w/{instance_code}/publication/copernicus',
            'namespace'  => 'Geonodo\Copernicus\Http\Controllers',
            'middleware' => ['web', 'auth', 'instance_user', 'scope-bouncer']
        ], function () {
            // Routes
            $this->loadRoutesFrom(__DIR__ . '/../routes/web.php');

            // Breadcrumbs
            // $this->loadRoutesFrom(__DIR__ . '/../routes/breadcrumbs.php');
        });
    }

    /**
     * Register the translations.
     *
     * @return void
     */
    protected function registerTranslations()
    {
        $this->loadTranslationsFrom(__DIR__ . '/../resources/lang', 'copernicus');
    }

    /**
     * Register the translations.
     *
     * @return void
     */
    protected function loadMigrations()
    {
        $this->loadMigrationsFrom(__DIR__ . '/../database/migrations');
    }

    /**
     * Publish the assets
     */
    protected function publishAssets()
    {
        $this->publishes([
            __DIR__ . '/../resources/assets/js/components' => base_path('resources/assets/js/vendor/copernicus'),
        ], 'assets');

        $this->publishes([
            __DIR__.'/../resources/images' => public_path('vendor/geonodo/copernicus'),
        ], 'public');
    }
}
