<?php

return [
    'menu' => [
        'title' => 'Copernicus',
        'details' => 'Integración con Copernicus'
    ],
    'index' => [
        'title' => 'Copernicus',
        'new_btn' => 'Nuevo',
    ],
];