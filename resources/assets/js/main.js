import Vue from 'vue';
import App from './App'
import VCalendar from "v-calendar";
import store from '@/Store'

Vue.config.productionTip = false;
Vue.use(VCalendar);

Vue.component('copernicus-app', App);

new Vue({
    el: '#copernicus-app',
    store
});
