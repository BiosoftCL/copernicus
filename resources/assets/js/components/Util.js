export const concateList = (items, concat='AND') => {
    const conjugator = items.length > 1 ? ` ${concat} ` : "";
    const itemList = `${items.slice(0, -1).join(` ${concat} `)}${conjugator}${items.slice(-1)}`;
    return itemList;
}

export const constructorList= (form) => {
    const query = []
    Object.keys(form).filter(col => form[col] !=null).forEach(col => query.push(`${col}:${form[col]}`))
    return query
}

export const getSubset = (obj, ...keys) => keys.reduce((a, c) => ({ ...a, [c]: obj[c] }), {});