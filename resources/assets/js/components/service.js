import axios from "axios";
import bus from '@/Bus';
import store from '@/Store'

axios.interceptors.request.use(function (config) {
    bus.$emit('loading', true)
    config.headers['X-CSRF-TOKEN'] = Laravel.csrfToken;
    return config;
});

axios.interceptors.response.use((response) => {
    bus.$emit('loading', false)
    return response;
});

/**
 * Guarda la configuración de los estilos
 * @param {Number} toc ID de la tabla de contenido
 * @param {Array} formData Configuración de capas
 */
function search(query,start, per_page, orderBy) {
    return axios.get(`copernicus/search`,{
            params: {
                query,
                start,
                per_page,
                orderBy
            }
        })
        .then(response => {
            store.dispatch('addData',response.data)
        });
}
function download(id) {
    return axios.get(`copernicus/download/${id}`, {
        responseType: 'arraybuffer',
    })
        .then(response => {
            var fileURL = window.URL.createObjectURL(new Blob([response.data]));
            var fileLink = document.createElement('a');

            fileLink.href = fileURL;
            fileLink.setAttribute('download', 'file.zip');
            document.body.appendChild(fileLink);
            fileLink.click();
        });
}

export { search, download}