import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const store = new Vuex.Store({
state: {
    showResult: true,
    showSearchAdvanced: false,
    data: {
    products: []
    }
},
mutations: {
    showResult (state, val) {
    state.showResult = val;
    // state.showSearchAdvanced = !state.showSearchAdvanced;
    },
    showSearchAdvanced (state, val) {
    state.showSearchAdvanced = val;
    },
    addData (state, data) {
    state.data= data
    }
},
getters: {
    getData: state => {
        return state.data
    },
    getProductByUuid: (state) => (uuid) => {
        return state.data.products.find(product => product.uuid === uuid);
    }
},
actions: {
    addData (context, data) {
        context.commit('addData',data)
        context.commit('showResult', true)
        if(context.state.showSearchAdvanced) {
            context.commit('showSearchAdvanced', false)
        }
    },
    showSearchAdvanced (context, val) {
        if(val && context.state.showResult) {
            context.commit('showResult', false)
        }
        context.commit('showSearchAdvanced', val)

    }
}
})
export default store