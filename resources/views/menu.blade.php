{{-- links de menú para incluir en list-group --}}
@component('components.menu.item', [
         'route' => route('copernicus.index', $instance),
         'image' => asset('img/menu-icons/csw-external.png'),
         'text' => __('copernicus::common.menu.title'),
         'details' => __('copernicus::common.menu.details')
     ])
@endcomponent
