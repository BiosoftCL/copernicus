<?php
/*
 * Skeleton breadcrumbs
 * ------------------
 */
Breadcrumbs::register('csw-harvesting::foo', function ($breadcrumbs) {
    $breadcrumbs->parent('production');
    $breadcrumbs->push(__('csw-harvesting::common.foo'), route('csw-harvesting.foo.index'));
});

Breadcrumbs::register('csw-harvesting::foo.create', function ($breadcrumbs) {
    $breadcrumbs->parent('csw-harvesting::foo');
    $breadcrumbs->push(__('csw-harvesting::common.create'), route('csw-harvesting.foo.create'));
});

Breadcrumbs::register('csw-harvesting::csw.show', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('csw-harvesting::csw');
    $breadcrumbs->push(__('csw-harvesting::common.show.title_breadcrumb'), route('csw-harvesting::csw.show', $id));
});