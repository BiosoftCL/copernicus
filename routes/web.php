<?php
/*
 * Skeleton routes
 * ------------------
 */

Route::group(['as' => 'copernicus.'], function () {
	Route::get('/', 'CopernicusController@index')->name('index');
	Route::get('/thumbnail/{uuid}', 'CopernicusController@getThumbnail')->name('thumbnail');
	Route::get('/quicklook/{uuid}', 'CopernicusController@getQuicklook')->name('thumbnail');
	Route::get('/download/{uuid}', 'CopernicusController@downloadFile')->name('thumbnail');
	Route::get('/search', 'CopernicusController@search')->name('search');
});